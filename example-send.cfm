<!--- instantiate the loggly component --->
<cfset variables.loggly = new loggly()>

<!--- example JSON file to send to Loggly: replace with your own. It is not necessary to XMLFormat this, the function will do that for you --->
<cfsavecontent variable="variables.jsonExample">
	{"type":"user","user":"Marcel van Langen","datetime":"2019-01-01 01:55:12.001","action":"insert log entry","url":"https://wwww.morloff.nl"}
</cfsavecontent>

<!--- Send the JSON file. If everything works out, than variables.sendResult contains '200 OK' --->
<cfset variables.sendResult = variables.loggly.sendLogLine(logData=variables.jsonExample)>

<cfdump var="#variables.sendResult#">