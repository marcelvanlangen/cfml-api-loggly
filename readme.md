# CFML Library for Loggly

This simple CFML Library contains everything you need to send or receive log lines to Loggly using their REST services.

## Installation

There really is no need to install anything. Take the following steps to make it work:

* Place loggly.cfc in the directory where you store your CFC's
* Go to loggly.com and create an account
* Make sure you copy the customerToken string (not the API token!)
* Go to loggly.cfc and change the value of the static customerToken to your value
* Take the code in example-send.cfm or example-receive.cfm and make it your own!

## Sending

All you need is a valid JSON file containing all the information that you want to send to Loggly. The example.cfm file contains a simple example. The example-send.cfm contains everything you need to get it working.

## Receiving

Just grab the example file example-receive.cfm. Just make sure you've entered your credentials in loggly.cfc, replace the query parameters with your own, and you're good to go!