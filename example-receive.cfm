<!--- instantiate the loggly component --->
<cfset variables.loggly = new loggly()>

<!--- query parameters. See Loggly documentation for help, default values and valid values --->
<cfset variables.q = "Marcel">
<cfset variables.from = "-10h">
<cfset variables.until = "-2h">
<cfset variables.order = "asc">
<cfset variables.size = "12">

<!--- Send the query and get a resultset back --->
<cfset variables.receiveResult = variables.loggly.receiveLogLines(q=variables.q,from=variables.from,until=variables.until,order=variables.order,size=variables.size)>

<cfdump var="#variables.receiveResult#">